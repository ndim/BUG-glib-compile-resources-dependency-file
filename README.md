Demonstrate bug in glib's `glib-compile-resources --dependency-file=DEPFILE`
============================================================================

The bug was filed at https://gitlab.gnome.org/GNOME/glib/-/issues/2829

Run `make` and observe the generated `foo-res.c.dep` dependency file:

    foo-res.xml: ./hello.txt

which makes no sense.

The `foo-res.c.dep` dependency file should actually look like

    foo-res.c: foo-res.xml ./hello.txt

which it does not as of version `glib-2.75.1` from git or
`glib-2.74.1` from Fedora 37 of the `glib-compile-resources` tool.
