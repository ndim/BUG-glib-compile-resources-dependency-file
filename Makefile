GLIB_COMPILE_RESOURCES = glib-compile-resources

GLIB_CFLAGS = $(shell pkg-config --cflags glib-2.0)
GLIB_LIBS   = $(shell pkg-config --libs   glib-2.0)

CFLAGS += $(GLIB_CFLAGS)
LDADD  += $(GLIB_LIBS)

GIO_CFLAGS = $(shell pkg-config --cflags gio-2.0)
GIO_LIBS   = $(shell pkg-config --libs   gio-2.0)

CFLAGS += $(GIO_CFLAGS)
LDADD  += $(GIO_LIBS)

.PHONY: all
all: foo

include foo-res.c.dep
foo-res.c.dep:
	: >> $@

foo-res.c: foo-res.xml
	$(GLIB_COMPILE_RESOURCES) --sourcedir=. --dependency-file=$@.dep --target=$@ --generate-source $<

foo: main.o foo-res.o
	$(LINK.c) -o $@ $^ $(LDADD)

.PHONY: clean
clean:
	rm -f foo *.o *.dep foo-res.c
