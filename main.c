#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <gio/gio.h>

int main(void)
{
  GBytes *bytes = g_resources_lookup_data("/hello.txt", 0, NULL);
  if (bytes == NULL) {
    printf("Could not load resource\n");
    exit(EXIT_FAILURE);
  }

  gsize size = -1;
  const char *const buf = g_bytes_get_data (bytes, &size);
  if (buf == NULL) {
    printf("Could not get data\n");
    exit(EXIT_FAILURE);
  }

  fwrite(buf, 1, size, stdout);

  g_bytes_unref(bytes);
  bytes = NULL;

  return 0;
}
